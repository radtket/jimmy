import React from 'react';
import Header from '../../components/Header';
import omg from './omg.mp4';

const FourOhFour = ({ posts }) => (
  <div>
    <Header video={omg}>
      <div className="title">
        <h1>404</h1>
      </div>
      <div className="summary">
        <p>That wasn't found.</p>
      </div>
    </Header>
  </div>
);

export default FourOhFour;
