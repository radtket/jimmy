import React from 'react';
import Article from '../../../components/Article';

export const data = {
  id: 'aapaconference',
  isWork: true,
  subtitle: 'aapa',
  title: 'Share your design work,<br/>clutter-free.',
  date: '2017-04-01',
  path: '/work/aapa-conference',
  external: 'https://aapa.org',
  excerpt: `Around Easter 2017 I decided to work on a simple webapp called Delivery to help designers share their work in progress. I wanted to create a hassle–free solution, which is reduced to the bare minimum of features and requires neither sign-up nor a credit card.`,
  details: [
    {
      title: 'Responsibility',
      description: 'Design & Development',
    },
    {
      title: 'Date',
      description: 'Q2 2017',
    },
    {
      title: 'Technology',
      description: 'Pug, jQuery, Sass',
    },
  ],
};

const WorksAapaConference = props => (
  <Article path={data.path} {...props}>
    <h1>AAPA</h1>
  </Article>
);

export default WorksAapaConference;
