import React from 'react';

const Block = ({ children, align, pull, vc, full, style, mobilePull }) => (
  <div
    style={style}
    className={`block ${align || ''} ${pull ? 'pull' : ''} ${vc ? 'vc' : ''} ${full ? 'full' : ''} ${
      mobilePull ? 'mobile-pull' : ''
    }`}
  >
    {children}
  </div>
);

export default Block;
