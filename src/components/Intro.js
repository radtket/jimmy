import React from 'react';

const Intro = ({ children, details }) => (
  <div className="intro">
    <ul>{details ? details.map((item, i) => <li key={i}>{item.description}</li>) : null}</ul>
  </div>
);

export default Intro;
