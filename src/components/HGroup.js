import React from 'react';

const HGroup = ({ title, subtitle, large }) => (
  <hgroup>
    <h6>{subtitle}</h6>
    {large ? <h1 dangerouslySetInnerHTML={{ __html: title }} /> : <p>{title}</p>}
  </hgroup>
);

export default HGroup;
