import React from 'react';
import Post from './Post';

const split = items => {
  const col1 = [];
  const col2 = [];

  items.map((item, i) => {
    if (i % 2 !== 0) {
      return col1.push(item);
    }
    return col2.push(item);
  });

  return (
    <div className="grid">
      <div className="col">{col1}</div>
      <div className="col">{col2}</div>
    </div>
  );
};

const preparePosts = posts => {
  const nodes = posts.map(post => {
    if (post.node.path !== '/404/') {
      return <Post key={post.node.data.path} post={post} />;
    }
    return false; // Added for eslint
  });

  return split(nodes);
};

const Posts = ({ posts }) => (
  <div id="work" className="posts">
    {preparePosts(posts)}
  </div>
);

export default Posts;
