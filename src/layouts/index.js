import React from 'react';
import PropTypes from 'prop-types';
import { observer, Provider } from 'mobx-react';
import { create } from 'mobx-persist';
import Img from 'gatsby-image';

import '../styles/style.scss';

import Topbar from '../components/Topbar';
import Footer from '../components/Footer';

import Store from '../stores/Store';

const store = new Store();

if (typeof window !== `undefined`) {
  const hydrate = create();

  hydrate('site', store).then(args => {
    console.log('hydrated!', args);
  });

  hydrate('articles', store).then(args => {
    console.log('hydrated!', args);
  });
}

@observer
class Template extends React.Component {
  constructor(props) {
    super();

    // Have to manually set the covers because github.com/gatsbyjs/gatsby/issues/2929
    const articles = props.data.allJsFrontmatter.edges.map(item => {
      item.node.data.cover = props.data[item.node.data.id];
      return item;
    });

    store.setArticles(articles);
    store.setSite(props.data.site);
  }

  render() {
    const { location, children, data } = this.props;

    return (
      <div>
        <Topbar title={data.site.siteMetadata.title} />
        <Provider store={store}>
          <main className="container">{children()}</main>
        </Provider>
        <Footer title={data.site.siteMetadata.title} />
      </div>
    );
  }
}

Template.propTypes = {
  children: PropTypes.func,
  location: PropTypes.object,
  route: PropTypes.object,
  data: PropTypes.object,
};

export default Template;

export const pageQuery = graphql`
  query IndexQuery {
    site {
      siteMetadata {
        title
      }
    }
    allJsFrontmatter(filter: { data: { isWork: { eq: true } } }, sort: { fields: [data___date], order: DESC }) {
      edges {
        node {
          data {
            id
            error
            title
            path
            devOnly
            details {
              title
              description
            }
            date
            excerpt
            contain
            background
            subtitle
            isWork
          }
        }
      }
    }
    # Covers for /work
    # Really nasty workaround, thanks to \`gatsby-transformer-javascript-static-exports\` taking
    # issue working together with \`gatsby-image\`. Issue for this: github.com/gatsbyjs/gatsby/issues/2929
    momenta: file(relativePath: { eq: "work/momenta/momenta.png" }) {
      ...Cover
    }
    delivery: file(relativePath: { eq: "work/delivery/delivery.png" }) {
      ...Cover
    }
    metroc: file(relativePath: { eq: "work/metroc/metroc.png" }) {
      ...Cover
    }
    aapaconference: file(relativePath: { eq: "work/aapa-conference/aapa.jpg" }) {
      ...Cover
    }
  }

  fragment Cover on File {
    childImageSharp {
      sizes(maxWidth: 1100, quality: 90) {
        ...GatsbyImageSharpSizes_withWebp
      }
    }
  }
`;
