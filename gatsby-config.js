const autoprefixer = require('autoprefixer');

module.exports = {
  siteMetadata: {
    title: 'Fabian Schultz',
    author: 'Fabian Schultz',
    siteUrl: `https://fabianschultz.com`,
  },
  plugins: [
    {
      resolve: `gatsby-plugin-nprogress`,
      options: {
        color: `tomato`,
        showSpinner: false,
      },
    },
    `gatsby-transformer-javascript-static-exports`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/pages`,
        name: 'pages',
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-postcss-sass`,
      options: {
        postCssPlugins: [autoprefixer()],
        precision: 5, // SASS default: 5
      },
    },
  ],
};
